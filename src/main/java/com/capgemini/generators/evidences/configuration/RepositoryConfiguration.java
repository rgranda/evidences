/**
 * RepositoryConfiguration.java 18-sep-2019
 *
 * Copyright 2019 CAPGEMINI.
 * Departamento de desarrollo Asturias
 */
package com.capgemini.generators.evidences.configuration;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * The Class RepositoryConfiguration.
 *
 * @author rgrandag
 */
@Data 
public class RepositoryConfiguration {
    
    private boolean sslVerify = Boolean.FALSE;
    
    private boolean enabled= Boolean.TRUE;
    
    List<RepositoryInfo> repositories = new ArrayList<>();

}
