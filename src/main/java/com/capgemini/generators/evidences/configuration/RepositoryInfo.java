/**
 * RepositoryInfo.java 18-sep-2019
 *
 * Copyright 2019 CAPGEMINI.
 * Departamento de desarrollo Asturias
 */
package com.capgemini.generators.evidences.configuration;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * The Class RepositoryInfo.
 *
 * @author rgrandag
 */
@Data
public class RepositoryInfo {
    
    private String url;
    
    private List<String> branches = new ArrayList<>();
    
    private String code;
    
    private boolean enabled = Boolean.TRUE;

}
