/**
 * GitConfiguration.java 14-sep-2019
 *
 * Copyright 2019 CAPGEMINI.
 * Departamento de desarrollo Asturias
 */
package com.capgemini.generators.evidences.configuration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * The Class AppConfiguration.
 *
 * @author rgrandag
 */
@Data
@Component
@ConfigurationProperties("app.scanner")
public class AppConfiguration {

    private Date since;

    private Date until;

    private String committer;

    private String pon;

    private List<String> blackList= new ArrayList<>();

    private List<String> jiraCodes= new ArrayList<>();
    
    private List<String> excludeCommits = new ArrayList<>();
    
    private List<String> excludeFiles = new ArrayList<>();

    private RepositoryConfiguration git;

    private RepositoryConfiguration svn;

}