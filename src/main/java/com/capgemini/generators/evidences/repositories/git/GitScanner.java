/**
 * GitScanner.java 14-sep-2019
 *
 * Copyright 2019 CAPGEMINI.
 * Departamento de desarrollo Asturias
 */
package com.capgemini.generators.evidences.repositories.git;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.revwalk.filter.AndRevFilter;
import org.eclipse.jgit.revwalk.filter.AuthorRevFilter;
import org.eclipse.jgit.revwalk.filter.CommitTimeRevFilter;
import org.eclipse.jgit.revwalk.filter.RevFilter;
import org.eclipse.jgit.storage.file.FileBasedConfig;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.util.FS;
import org.eclipse.jgit.util.SystemReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.generators.evidences.configuration.AppConfiguration;
import com.capgemini.generators.evidences.configuration.RepositoryInfo;
import com.capgemini.generators.evidences.repositories.dtos.CommitDto;
import com.capgemini.generators.evidences.repositories.dtos.PdfInformationDto;
import com.capgemini.generators.evidences.repositories.mappers.ProyectInformationMapper;
import com.capgemini.generators.evidences.utils.EvidenceUtils;

/**
 * Instancia un nuevo git scanner.
 *
 * @author rgrandag
 */
@Component
public class GitScanner {

    private static final Logger LOG = LoggerFactory.getLogger(GitScanner.class);

    @Autowired
    private AppConfiguration appConfiguration;

    @Autowired
    private EvidenceUtils util;

    @Autowired
    private ProyectInformationMapper proyectInformationMapper;

    private String username;

    private String password;

    /**
     * Obtiene pdf information.
     *
     * @param username
     *            username
     * @param password
     *            password
     * @return pdf information
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public PdfInformationDto getPdfInformation(final String username, final String password) throws IOException {
        if (!this.appConfiguration.getGit().isEnabled()) {
            return null;
        }
        this.username = username;
        this.password = password;


        final PdfInformationDto pdfInformationDto = new PdfInformationDto();
        pdfInformationDto.setUsername(this.username);
        pdfInformationDto.setDate(EvidenceUtils.format(this.appConfiguration.getSince(), EvidenceUtils.FECHA_MES_ANIO));

        final Map<String, List<CommitDto>> gits = this.obtainCommits();

        pdfInformationDto.getProjects().addAll(this.proyectInformationMapper.toDto(gits.entrySet()));

        return pdfInformationDto;
    }

    /**
     * Obtain commits.
     *
     * @return the map
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private Map<String, List<CommitDto>> obtainCommits() throws IOException {
        final Map<String, List<CommitDto>> map = new HashMap<>();
        for (final RepositoryInfo gitRepository : this.appConfiguration.getGit().getRepositories()) {
            if (gitRepository.isEnabled()) {
                for (final String branch : gitRepository.getBranches()) {
                    this.obtainCommits(gitRepository, map, branch);
                }
            }
        }
        return map;
    }

    /**
     * Obtain git commits.
     *
     * @param gitRepository
     *            git repository
     * @param map
     *            map
     * @return the map
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private Map<String, List<CommitDto>> obtainCommits(final RepositoryInfo gitRepository,
            final Map<String, List<CommitDto>> map, final String branch) throws IOException {

        final Git git = this.getGitRepository(gitRepository.getUrl(), branch);
        final Repository repository = git.checkout().setName(branch).getRepository();
        LOG.info("Getting data from {} ...", repository);
        final List<Ref> allRefs = repository.getRefDatabase().getRefs();
        final RevWalk walk = new RevWalk(repository);
        for (final Ref ref : allRefs) {
            walk.markStart(walk.parseCommit(ref.getObjectId()));
        }

        final RevFilter between = CommitTimeRevFilter.between(this.appConfiguration.getSince(),
                this.appConfiguration.getUntil());
        final RevFilter author = AuthorRevFilter.create(this.username);

        final RevFilter filter = AndRevFilter.create(between, author);
        walk.setRevFilter(filter);


        for (final RevCommit commitData : walk) {
            final CommitDto gitCommit = new CommitDto(commitData.getName(), commitData.getAuthorIdent().getName(),
                    new Date(Long.parseLong(String.valueOf(commitData.getCommitTime())) * 1000L),
                    commitData.getShortMessage(), commitData.getFullMessage());
            if (commitData.getParents().length > 0) {
                final ObjectReader reader = git.getRepository().newObjectReader();
                final CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
                oldTreeIter.reset(reader, commitData.getTree());
                final CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
                newTreeIter.reset(reader, commitData.getParents()[0].getTree());

                final ByteArrayOutputStream out = new ByteArrayOutputStream();
                final DiffFormatter diffFormater = new DiffFormatter(out);
                diffFormater.setRepository(git.getRepository());
                final List<DiffEntry> entries = diffFormater.scan(newTreeIter, oldTreeIter);

                entries.removeIf(item -> this.util.checkFileExclude(
                        item.getNewPath().equals("/dev/null") ? item.getOldPath() : item.getNewPath()));

                gitCommit.setFiles(this.util.recoveryChangedFiles(entries));
                diffFormater.format(entries);
                diffFormater.close();
                gitCommit.setChanges(this.util.getLines(out));
            }

            List<CommitDto> commitsGit = map.get(gitRepository.getCode());
            if (commitsGit == null) {
                commitsGit = new ArrayList<>();
            }
            commitsGit.add(gitCommit);
            map.put(gitRepository.getCode(), commitsGit);
        }
        walk.reset();
        walk.close();
        return map;

    }



    /**
     * Obtiene git repository.
     *
     * @param gitRepository
     *            git repository
     * @return git repository
     */
    private Git getGitRepository(final String url, final String branch) {

        final CredentialsProvider cp = new UsernamePasswordCredentialsProvider(this.username, this.password);
        Path localPath;
        try {
            LOG.info("Connection to repository {} and branch {}...", url, branch);
            localPath = Files.createTempDirectory("GitRepository");
            final CloneCommand clone = Git.cloneRepository().setURI(url).setBranch(branch).setCredentialsProvider(cp)
                    .setDirectory(localPath.toFile());
            this.disableSSLVerify(URI.create(url));
            return clone.call();

        } catch (final Exception excep) {
            LOG.error(excep.getLocalizedMessage(), excep);
            return null;
        }

    }

    /**
     * Disable SSL verify.
     *
     * @param gitServer
     *            git server
     * @throws Exception
     *             de exception
     */
    private void disableSSLVerify(final URI gitServer) throws Exception {
        if (gitServer.getScheme().equals("https") && !this.appConfiguration.getGit().isSslVerify()) {
            final FileBasedConfig config = SystemReader.getInstance().openUserConfig(null, FS.DETECTED);
            synchronized (config) {
                config.load();
                config.setBoolean("http", "https://" + gitServer.getHost() + ':'
                        + (gitServer.getPort() == -1 ? 443 : gitServer.getPort()), "sslVerify", false);
                config.save();
            }
        }
    }

}
