/**
 * FileDiffDto.java 18-sep-2019
 *
 * Copyright 2019 CAPGEMINI.
 * Departamento de desarrollo Asturias
 */
package com.capgemini.generators.evidences.repositories.dtos;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * The Class FileDiffDto.
 *
 * @author rgrandag
 */
@Data
@RequiredArgsConstructor
public class FileDiffDto {
    @NonNull
    private String path;
    
    private List<String> lines = new ArrayList<>();
    
    /**
     * Anade el line.
     *
     * @param line line
     */
    public void addLine(String line) {
        
        this.lines.add(line);
        
    }
    

}
