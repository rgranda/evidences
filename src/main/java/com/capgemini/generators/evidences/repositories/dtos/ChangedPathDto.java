/**
 * ChangedPathDto.java 18-sep-2019
 *
 * Copyright 2019 CAPGEMINI.
 * Departamento de desarrollo Asturias
 */
package com.capgemini.generators.evidences.repositories.dtos;

import com.capgemini.generators.evidences.enums.FileType;

import lombok.Data;

/**
 * The Class ChangedPathDto.
 *
 * @author rgrandag
 */
@Data
public class ChangedPathDto {

    private FileType type;

    private String path;

    private String copyPath;

    private Long copyRevision;

    private FileDiffDto file = null;


    /**
     * Instancia un nuevo changed path.
     *
     * @param type type
     * @param path path
     * @param copyPath copy path
     * @param copyRevision copy revision
     */
    public ChangedPathDto(final FileType type, final String path, final String copyPath, final long copyRevision)
    {
        this.type = type;
        this.path = path;
        this.copyPath = copyPath;
        this.copyRevision = copyRevision;
    }
    
    /**
     * Instancia un nuevo changed path.
     *
     * @param type type
     * @param path path
     * @param copyPath copy path
     */
    public ChangedPathDto(final FileType type, final String path, final String copyPath)
    {
        this.type = type;
        this.path = path;
        this.copyPath = copyPath;
    }

}
