/**
 * FileDiffMapper.java 19-sep-2019
 *
 * Copyright 2019 INDITEX.
 * Departamento de Sistemas
 */
package com.capgemini.generators.evidences.repositories.mappers;

import org.mapstruct.Mapper;

/**
 * The Interface FileDiffMapper.
 *
 * @author capgemini
 */
@Mapper
public interface FileDiffMapper {

}
