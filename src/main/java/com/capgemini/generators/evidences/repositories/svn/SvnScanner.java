/**
 * SvnScanner.java 15-sep-2019
 *
 * Copyright 2019 CAPGEMINI.
 * Departamento de desarrollo Asturias
 */
package com.capgemini.generators.evidences.repositories.svn;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNDiffClient;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import com.capgemini.generators.evidences.configuration.AppConfiguration;
import com.capgemini.generators.evidences.configuration.RepositoryInfo;
import com.capgemini.generators.evidences.enums.FileType;
import com.capgemini.generators.evidences.repositories.dtos.ChangedPathDto;
import com.capgemini.generators.evidences.repositories.dtos.CommitDto;
import com.capgemini.generators.evidences.repositories.dtos.FileDiffDto;
import com.capgemini.generators.evidences.repositories.dtos.PdfInformationDto;
import com.capgemini.generators.evidences.repositories.mappers.ProyectInformationMapper;
import com.capgemini.generators.evidences.utils.EvidenceUtils;

/**
 * The Class SvnScanner.
 *
 * @author rgrandag
 */
@Component
public class SvnScanner {

    private static final Logger LOG = LoggerFactory.getLogger(SvnScanner.class);

    @Autowired
    private AppConfiguration appConfiguration;

    @Autowired
    private EvidenceUtils util;

    @Autowired
    private ProyectInformationMapper proyectInformationMapper;

    private String username;

    private String password;

    private SVNRepository repository;

    private SVNClientManager client;

    /**
     * Obtiene pdf information.
     *
     * @param username
     *            username
     * @param password
     *            password
     * @return pdf information
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws SVNException
     *             de SVN exception
     */
    public PdfInformationDto getPdfInformation(final String username, final String password)
            throws IOException, SVNException {

        if (!this.appConfiguration.getSvn().isEnabled()) {
            return null;
        }

        this.username = username;
        this.password = password;

        final PdfInformationDto pdfInformationDto = new PdfInformationDto();
        pdfInformationDto.setUsername(this.username);
        pdfInformationDto.setDate(EvidenceUtils.format(this.appConfiguration.getSince(), EvidenceUtils.FECHA_MES_ANIO));

        final Map<String, List<CommitDto>> svns = this.obtainCommits();

        pdfInformationDto.getProjects().addAll(this.proyectInformationMapper.toDto(svns.entrySet()));

        return pdfInformationDto;
    }

    /**
     * Obtain commits.
     *
     * @return the map
     * @throws SVNException
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private Map<String, List<CommitDto>> obtainCommits() throws SVNException, IOException {
        final Map<String, List<CommitDto>> map = new HashMap<>();
        for (final RepositoryInfo repo : this.appConfiguration.getSvn().getRepositories()) {
            if (repo.isEnabled()) {
                for (final String branch : repo.getBranches()) {
                    this.obtainCommits(repo, map, branch);
                }
            }
        }
        return map;
    }

    /**
     * Obtain commits.
     *
     * @param repository
     *            repository
     * @param map
     *            map
     * @throws SVNException
     * @throws IOException
     */
    @SuppressWarnings({ "rawtypes" })
    private void obtainCommits(final RepositoryInfo repository, final Map<String, List<CommitDto>> map,
            final String branch)
                    throws SVNException, IOException {

        final SVNURL url = SVNURL.parseURIEncoded(repository.getUrl() + '/' + branch);
        this.getSVNRepo(url);
        LOG.info("Getting data from {} ...", repository);
        final long startRevision = this.repository.getDatedRevision(this.appConfiguration.getSince());
        final long endRevision = this.repository.getDatedRevision(this.appConfiguration.getUntil());

        final List<FileDiffDto> diff = this.getDifferences(url, startRevision, endRevision);

        final Collection logEntries = this.repository.log(new String[] { "" }, null, startRevision, endRevision, true,
                true);

        for (final Iterator entries = logEntries.iterator(); entries.hasNext();) {

            if (map.get(repository.getCode()) == null) {
                map.put(repository.getCode(), new ArrayList<>());
            }
            final SVNLogEntry logEntry = (SVNLogEntry) entries.next();
            if ((logEntry.getAuthor() != null) && logEntry.getAuthor().equals(this.username)
                    && this.util.containsJiraCodes(logEntry.getMessage())) {
                final CommitDto rev = new CommitDto(String.valueOf(logEntry.getRevision()), logEntry.getAuthor(),
                        logEntry.getDate(),
                        logEntry.getMessage(), logEntry.getMessage());

                if (logEntry.getChangedPaths().size() > 0) {
                    final Set<String> changedPathsSet = logEntry.getChangedPaths().keySet();
                    for (final String string : changedPathsSet) {
                        final SVNLogEntryPath entryPath = logEntry.getChangedPaths().get(string);

                        if (!this.util.checkFileExclude(entryPath.getPath())) {

                            final ChangedPathDto cp = new ChangedPathDto(FileType.getType(entryPath.getType()),
                                    entryPath.getPath(),
                                    entryPath.getPath(), entryPath.getCopyRevision());
                            final Map<String, List<String>> lines = new HashMap<>();

                            diff.removeIf(item -> this.util.checkFileExclude(item.getPath()));

                            for (final FileDiffDto file : diff) {

                                if (entryPath.getPath().contains(file.getPath())) {
                                    cp.setFile(file);
                                }
                                lines.put(file.getPath(), file.getLines());
                            }

                            rev.setChanges(lines);
                            rev.addChangedPath(cp);
                        }
                    }
                }
                map.get(repository.getCode()).add(rev);
            }
        }
    }


    /**
     * Obtiene differences.
     *
     * @param url
     *            url
     * @param startRevision
     *            start revision
     * @param endRevision
     *            end revision
     * @return differences
     * @throws SVNException
     *             de SVN exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private List<FileDiffDto> getDifferences(final SVNURL url, final long startRevision, final long endRevision)
            throws SVNException, IOException {

        final List<FileDiffDto> diff = new ArrayList<>();

        final SVNDiffClient diffClient = this.client.getDiffClient();

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        diffClient.doDiff(url, SVNRevision.create(startRevision), url, SVNRevision.create(endRevision),
                SVNDepth.UNKNOWN, Boolean.TRUE, baos);

        final String diffRaw = baos.toString("UTF-8");

        final String[] lines = diffRaw.split("\\r?\\n");
        String[] arrayOfString1;
        final int j = (arrayOfString1 = lines).length;
        for (int i = 0; i < j; i++) {
            final String line = arrayOfString1[i];
            if (line != null) {
                final String parsedLine = StringUtils.stripAccents(line);
                if (parsedLine.matches("\\A\\p{ASCII}*\\z")) {
                    final String lineCode = this.util.removeInvalidCharacters(line);
                    if (lineCode.contains("Index: ")) {
                        diff.add(new FileDiffDto("/" + lineCode.replaceAll("Index: ", "")));
                    } else if (!diff.isEmpty()) {
                        diff.get(diff.size() - 1).addLine(this.util.removeInvalid(lineCode));
                    }
                }
            }
        }
        baos.close();
        return diff;
    }

    /**
     * Obtiene SVN repo.
     *
     * @param repository
     *            repository
     * @return
     * @return SVN repo
     * @throws SVNException
     */
    private void getSVNRepo(final SVNURL url) throws SVNException {
        DAVRepositoryFactory.setup();
        SVNRepositoryFactoryImpl.setup();
        FSRepositoryFactory.setup();
        LOG.info("Connection to {} ...", url);

        this.repository = SVNRepositoryFactory.create(url);

        final ISVNAuthenticationManager auth = SVNWCUtil.createDefaultAuthenticationManager(this.username,
                this.password.toCharArray());

        this.repository.setAuthenticationManager(auth);
        this.client = SVNClientManager.newInstance(SVNWCUtil.createDefaultOptions(Boolean.TRUE), auth);

    }

}
