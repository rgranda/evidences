/**
 * ProyectInformationMapper.java 19-sep-2019
 *
 * Copyright 2019 INDITEX.
 * Departamento de Sistemas
 */
package com.capgemini.generators.evidences.repositories.mappers;

import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.capgemini.generators.evidences.repositories.dtos.CommitDto;
import com.capgemini.generators.evidences.repositories.dtos.ProyectInformationDto;



/**
 * The Interface ProyectInformationMapper.
 *
 * @author capgemini
 */
@Mapper(imports = {})
public interface ProyectInformationMapper {

    /**
     * To dto.
     *
     * @param git
     *            git
     * @return the proyect information dto
     */
    @Mapping(source = "key", target = "name")
    @Mapping(source = "value", target = "commits")
    ProyectInformationDto toDto(Entry<String, List<CommitDto>> git);

    /**
     * To dto.
     *
     * @param entrySet
     *            entry set
     * @return the collection
     */
    Collection<ProyectInformationDto> toDto(Set<Entry<String, List<CommitDto>>> entrySet);

}
