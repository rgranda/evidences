/**
 * ProyectInformation.java 15-sep-2019
 *
 * Copyright 2019 CAPGEMINI.
 * Departamento de desarrollo Asturias
 */
package com.capgemini.generators.evidences.repositories.dtos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * Instancia un nuevo proyect information.
 */
@Data
public class ProyectInformationDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    
    private List<CommitDto> commits = new ArrayList<>();

}
