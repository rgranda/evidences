/**
 * PdfInformationDto.java 19-sep-2019
 *
 * Copyright 2019 CAPGEMINI.
 * Departamento de desarrollo Asturias
 */
package com.capgemini.generators.evidences.repositories.dtos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * The Class PdfInformationDto.
 *
 * @author rgrandag
 */
@Data
public class PdfInformationDto implements Serializable{

    private static final long serialVersionUID = 1L;

    private String username;
    
    private String date;
    
    private List<ProyectInformationDto> projects = new ArrayList<>();
    
    
}
