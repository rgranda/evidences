/**
 * Application.java 14-sep-2019
 *
 * Copyright 2019 CAPGEMINI.
 * Departamento de desarrollo Asturias
 */
package com.capgemini.generators.evidences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationHome;

import com.capgemini.generators.evidences.pdf.PdfGenerator;
import com.capgemini.generators.evidences.repositories.dtos.PdfInformationDto;
import com.capgemini.generators.evidences.repositories.git.GitScanner;
import com.capgemini.generators.evidences.repositories.svn.SvnScanner;

/**
 * The Class Application.
 *
 * @author rgrandag
 */
@SpringBootApplication
public class Application implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    @Autowired
    private PdfGenerator pdfGenerator;

    @Autowired
    private GitScanner gitScanner;

    @Autowired
    private SvnScanner svnScanner;

    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run(final String... args) throws Exception {
        if (args.length < 2) {
            LOG.error("** Invalid arguments usage: java -jar evidences-generator-0.0.1.jar <user> <pass>");
        }
        if (args.length >= 2) {
            try {
                final ApplicationHome home = new ApplicationHome(Application.class);

                final PdfInformationDto gitInfo = this.gitScanner.getPdfInformation(args[0], args[1]);
                final PdfInformationDto svnInfo = this.svnScanner.getPdfInformation(args[0], args[1]);
                this.pdfGenerator.generateReport(gitInfo, home.getDir().getAbsoluteFile());
                this.pdfGenerator.generateReport(svnInfo, home.getDir().getAbsoluteFile());
                LOG.info("FINISH!!");

            } catch (final Exception excep) {
                LOG.error(excep.getMessage(), excep);
            }
        }
    }

}
